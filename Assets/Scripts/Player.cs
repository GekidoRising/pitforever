﻿//~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
//Player.cs                 
//
//by: Nicholas MacDonald
//
//Controls the player movement
//~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*

using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour 
{
    //All of the different states the player can be in
    enum PlayerState
    {
        Grounded,
        Airborne,
        Falling,
        Climbing,
        Swinging
    };

    public float force = 2000f; //The amount of force to push the player around wtih
    public float maxSpeed = 5f; //Max movement speed
    public float dampenForce = 100f; //The amount of force to stop the player with when they aren't moving (to prevent sliding)
    public float jumpForce = 3000f; //The amount of upward force used for jumping
    public float airControl = 0.5f; //How much force they can apply to themselves when in the air
    public float climbForce = 1000f; //Force applied to climb a ladder
    public float maxClimbSpeed = 2f; //Maximum climb speed
    public Vector3 exitForce; //The force used to jump off a ladder
    public int health = 2000; //Amount of health
    public int dps = 60; //The amount of damage the player takes when they touch a barrel
    public int lives = 3; //Life count

    private Rigidbody body;
    private AudioSource jumpSound;
    private AudioSource fallSound;
    private AudioSource hitSound;
    private TextMesh display;
    private GameManager manager;

    private PlayerState state = PlayerState.Airborne;
    private bool canClimb = false;
    private Vector3 velocityBuffer; //The player's velocity before they touched a barrel
    private float damageTick = 0f;

    private GameObject attachedRope = null;

    private Coroutine textFadeCoroutine;

	//We acquire all of the relevant components in the player as well as the manager
	void Start ()
    {
        body = GetComponent<Rigidbody>();

        AudioSource[] sources = GetComponents<AudioSource>();

        jumpSound = sources[0];
        fallSound = sources[1];
        hitSound = sources[2];

        display = GetComponentInChildren<TextMesh>();

        manager = FindObjectOfType<GameManager>();
	}

    void OnCollisionStay(Collision info)
    {
        //If the player touches with the ground and is not climbing, or their moving downwards on a ladder
        if (info.gameObject.tag == "Ground" && (state != PlayerState.Climbing || Input.GetAxisRaw("Vertical") < 0f))
        {
            //The player becomes grounded
            state = PlayerState.Grounded;
            body.useGravity = true;
        }
    }

    void OnCollisionExit(Collision info)
    {
        //If the player stops touching the ground and they aren't climbing
        if (info.gameObject.tag == "Ground" && state != PlayerState.Climbing)
        {
            //We consider them airborne
            state = PlayerState.Airborne;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        //If the player touches a drop trigger
        if (other.tag == "Drop")
        {
            //Then they start falling straight downwards
            state = PlayerState.Falling;

            fallSound.Play();

            Vector3 temp = body.velocity;
            temp.x = 0f;
            body.velocity = temp;
        }
        //If they're touching a ladder then they'll be ready to climb it
        else if (other.tag == "Ladder")
        {
            canClimb = true;
        }
        //If they touch a barrel, they'll freeze (or at least try to. It doesn't really work as intended)
        else if (other.tag == "Barrel")
        {
            velocityBuffer = body.velocity;
            body.velocity = Vector3.zero;
        }
        //If they touch a death trigger, we tell the manager that they're dying
        else if (other.tag == "Death")
        {
            manager.KillPlayer();
        }
        //Finally, if they touch a rope, we attach them to it
        else if(other.tag == "Rope")
        {
            attachedRope = other.gameObject;
            state = PlayerState.Swinging;
            body.isKinematic = true;
        }
    }

    void OnTriggerStay(Collider other)
    {
        //While we're inside a barrel
        if (other.tag == "Barrel")
        {
            //We increment a timer
            damageTick += Time.fixedDeltaTime;

            //While the timer is higher than zero, we decrement a 60th of a second from it as well as one tick from our health
            while(damageTick > 0.0f)
            {
                damageTick -= 1f / dps;
                health--;
                hitSound.Play();
            }

            Write(health.ToString());
        }
    }

    void OnTriggerExit(Collider other)
    {
        //If we leave a ladder trigger
        if (other.tag == "Ladder")
        {
            //We can no longer climb it
            canClimb = false;

            //If we were climbing it
            if(state == PlayerState.Climbing)
            {
                //We tell the ladder to reset its drop trigger
                Ladder ladder = other.gameObject.GetComponent<Ladder>();
                ladder.ResetDrop();

                //And then we reactivate the physics on the player
                body.useGravity = true;
                body.AddForce(exitForce);
                state = PlayerState.Airborne;
                jumpSound.Play();
            }
        }
        //If we exit a barrel, we (try to) reset the player's velocity.
        if (other.tag == "Barrel" && state != PlayerState.Falling)
        {
            body.velocity = velocityBuffer;
        }
    }

    void Update()
    {
        //If the player is swinging
        if (state == PlayerState.Swinging)
        {
            //We update their position to the end of the rope
            transform.position = attachedRope.transform.position;

            //If they jump off it
            if (Input.GetButton("Jump"))
            {
                //We reactivate their physics and cause them to jump upward
                body.isKinematic = false;

                body.AddForce(transform.up * jumpForce);
                jumpSound.Play();

                state = PlayerState.Airborne;
            }
        }
    }

    void FixedUpdate()
    {    
        //If the player isn't falling or swinging
        if (state != PlayerState.Falling && state != PlayerState.Swinging)
        {
            //If they're on the ground
            if (state == PlayerState.Grounded)
            {
                //If they press upward and are by a ladder
                if (Input.GetAxisRaw("Vertical") > 0f && canClimb)
                {
                    //We disable our gravity
                    state = PlayerState.Climbing;
                    body.useGravity = false;

                    Vector3 temp = body.velocity;
                    temp.x = 0f;
                    body.velocity = temp;
                }
                //If they aren't climbing, but pressing the jump button
                else if(Input.GetButton("Jump"))
                {
                    //We jump!
                    body.AddForce(transform.up * jumpForce);
                    jumpSound.Play();
                }
            }

            //If we're climbing
            if (state == PlayerState.Climbing)
            {
                float verticalAxis = Input.GetAxisRaw("Vertical");

                //If we're pressing up or down, we apply a climb force in that direction
                if (verticalAxis != 0f)
                {
                    body.AddForce(transform.up * force * verticalAxis);
                }
                //Otherwise, we dampen our movement to a stop
                else
                {
                    body.AddForce(transform.up * -body.velocity.y * dampenForce);
                }

                //We also cap our max speed to ensure that we don't blast up or down the ladder
                if (body.velocity.y > maxClimbSpeed)
                {
                    Vector3 temp = body.velocity;
                    temp.y = maxClimbSpeed;
                    body.velocity = temp;
                }
                else if (body.velocity.y < -maxClimbSpeed)
                {
                    Vector3 temp = body.velocity;
                    temp.y = -maxClimbSpeed;
                    body.velocity = temp;
                }
            }
            else
            {
                //At this point, if we aren't climbing, swinging or falling, we can only be grounded or airborne, thus, we can move.
                float horizontalAxis = Input.GetAxisRaw("Horizontal");

                //We apply a force in the direction we move in
                if (horizontalAxis != 0f)
                {
                    float multiplier = (state == PlayerState.Grounded) ? 1.0f : airControl;
                    body.AddForce(transform.right * force * horizontalAxis * multiplier);
                }
                //Or we dampen our movement so we're not sliding around everywhere
                else if(state != PlayerState.Airborne)
                {
                    body.AddForce(transform.right * -body.velocity.x * dampenForce);
                }

                //And we cap our speed
                if (body.velocity.x > maxSpeed)
                {
                    Vector3 temp = body.velocity;
                    temp.x = maxSpeed;
                    body.velocity = temp;
                }
                else if (body.velocity.x < -maxSpeed)
                {
                    Vector3 temp = body.velocity;
                    temp.x = -maxSpeed;
                    body.velocity = temp;
                }
            }          
        }
    }

    //Helper method to write text to the little floaty text above the Player
    void Write(string phrase)
    {
        if (textFadeCoroutine != null)
        {
            StopCoroutine(textFadeCoroutine);
        }

        textFadeCoroutine = StartCoroutine(WriteToDisplay(phrase));
    }

    //A coroutine to fade out the text after a short amount of time
    IEnumerator WriteToDisplay(string phrase)
    {
        display.text = phrase;

        Color c = display.color;
        c.a = 1.0f;
        display.color = c;

        yield return new WaitForSeconds(3.0f);

        float timer = 1.0f;

        while (timer > 0.0f)
        {
            timer -= Time.deltaTime;

            Color temp = display.color;
            temp.a = timer;
            display.color = temp;

            yield return null;
        }
    }

    //Respawn the character, resetting their movement speed and writing how many lives remain
    public void Respawn()
    {
        body.velocity = Vector3.zero;
        lives--;

        if (lives != 1)
        {
            Write(lives + " lives");
        }
        else
        {
            Write("1 life");
        }
    }
}

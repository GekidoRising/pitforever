﻿//~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
//Barrel.cs                 
//
//by: Nicholas MacDonald
//
//Rolls a barrel across the stage that deals contact damage to the player
//~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*

using UnityEngine;
using System.Collections;

public class Barrel : MonoBehaviour
{
    public float rollDistance = 30f; //The approximate distance the barrel will roll to cross the entire stage (or at least what the camera sees).
    public float speed = 5f;
    public float rotationalSpeed = 360f;
    public float delay = 0f;

    private float start, end;

    void Awake()
    {
        start = rollDistance / 2f;
        end = -rollDistance / 2f;
    }

    //Calculate the start and ends points relative to the stage
    void Start ()
    {     
        
    }

    //Reset the barrel to the right side of the stage
    public void Reset()
    {
        transform.localPosition = new Vector3(start, 1.27f, -0.87f);
    }

	void Update ()
    {
        //If there is any delay, the barrel is kept still
        if (delay > 0f)
        {
            delay -= Time.deltaTime;
        }
        else
        {
            //Once the delay has passed, the barrel starts rotating and moving forward
            transform.position += Vector3.left * speed * Time.deltaTime;
            transform.Rotate(0f, rotationalSpeed * Time.deltaTime, 0f);

            //If the barrel reaches the end, it's reset back to the start.
            if (transform.localPosition.x < end)
            {
                Reset();
            }
        }
	}
}

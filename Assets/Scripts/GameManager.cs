﻿//~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
//GameManager.cs                 
//
//by: Nicholas MacDonald
//
//Manages interactions between multiple game objects to reduce spaghetti code
//~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Player player;
    public Camera cam;

    public GameObject[] stages;
    public int currentStage;

    public AudioSource nextLevelSound;
    public AudioSource deathSound;
    public AudioSource respawnSound;
    public AudioSource gameOverSound;

    private float startingX, nextX;

    private StageGenerator generator;

    private GameObject generatedStage;

    //Based on the current stage selected, the player and camera are teleported to the proper stage
    void Start ()
    {
        startingX = player.transform.position.x;

        nextX = startingX + (currentStage + 1) * 100f;

        for(int j = 0; j < stages.Length; j++)
        {
            stages[j].SetActive(j == currentStage);
        }

        player.transform.position = new Vector3(-21f + 100f * currentStage, 2f, 0f);

        Vector3 pos = cam.transform.position;
        pos.x = 100f * currentStage;
        cam.transform.position = pos;

        //We also fetch the level generator for later
        generator = FindObjectOfType<StageGenerator>();
    }
	
    //Teleport the player over to the next level
	public void NextLevel()
    {
        //Every level is spaced exactly 100 unitys away from each other so we just need to move the
        //player and camera over
        Vector3 pos = player.transform.position;
        pos.x = nextX;
        pos.y += 0.5f;
        player.transform.position = pos;

        nextX += 100f;

        pos = cam.transform.position;
        pos.x += 100f;
        cam.transform.position = pos;

        //If the current stage was premade
        if (currentStage < stages.Length)
        {
            //We delete it from the array
            Destroy(stages[currentStage], 1.0f);
        }
        else
        {
            //Otherwise, we delete the generated stage. We wait a second because deleting colliders seems not to
            // trigger OnColliderExit events which throws off the player's jump code
            Destroy(generatedStage, 1.0f);
        }

        //We increment the stage counter
        currentStage++;

        //If the next stage is premade
        if (currentStage < stages.Length)
        {
            //We set it to active
            stages[currentStage].SetActive(true);
        }
        else
        {
            //Otherwise, we generate a brand new stage 100 units over to the side
            generatedStage = generator.GenerateStage();
            generatedStage.transform.position = new Vector3(currentStage * 100f, 0f, 0f);
        }

        nextLevelSound.Play();
    }

    //Player kill coroutines
    public void KillPlayer()
    {
        if (player.lives > 0)
        {
            //If the player still has lives left, we play a little tune and then reset the player to their original position
            StartCoroutine(ResetPlayer());
        }
        else
        {
            //Otherwise, we play a different jaunt and reload the scene
            StartCoroutine(ResetGame());
        }
    }

    //If the player still has lives left, we play a little tune and then reset the player to their original position
    IEnumerator ResetPlayer()
    {
        deathSound.Play();

        yield return new WaitForSeconds(2f);

        player.transform.position = new Vector3(-21f + 100f * currentStage, 2f, 0f);
        player.Respawn();

        respawnSound.Play();
    }

    //Otherwise, we play a different jaunt and reload the scene
    IEnumerator ResetGame()
    {
        gameOverSound.Play();

        yield return new WaitForSeconds(2f);

        SceneManager.LoadScene(0);
    }
}

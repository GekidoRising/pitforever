﻿//~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
//ResetDrop.cs                 
//
//by: Nicholas MacDonald
//
//[DEPRECATED] Resets all drop triggers for the entire stage when the player touches the upper ground level
//~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*

using UnityEngine;
using System.Collections;

public class ResetDrop : MonoBehaviour {

    public Drop[] drops;

	// Use this for initialization
	void Start () {
	
	}

    void OnCollisionEnter(Collision info)
    {
       if(info.gameObject.tag == "Player")
        {
            foreach(var drop in drops)
            {
                drop.gameObject.SetActive(true);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}

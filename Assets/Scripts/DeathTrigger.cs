﻿//~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
//DeathTrigger.cs                 
//
//by: Nicholas MacDonald
//
//Draws a gizmo for the death trigger so that it's visible
//~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*

using UnityEngine;
using System.Collections;

public class DeathTrigger : MonoBehaviour
{
    //Visualizes the death trigger as a black cube
    void OnDrawGizmos()
    {
        Gizmos.color = new Color(0f, 0f, 0f, 0.5f);
        Gizmos.DrawCube(transform.position, transform.lossyScale);

        Gizmos.color = new Color(0.2f, 0.2f, 0.2f, 1f);
        Gizmos.DrawWireCube(transform.position, transform.lossyScale);
    }

    //I really like gizmos
}

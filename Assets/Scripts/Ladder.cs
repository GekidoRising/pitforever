﻿//~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
//Ladder.cs                 
//
//by: Nicholas MacDonald
//
//Sets a climbing zone for the player
//~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*

using UnityEngine;
using System.Collections;

public class Ladder : MonoBehaviour
{
    //Stores the drop trigger above the ladder
    public Drop dropTrigger;

    //When the player enter's the ladder's trigger, we disable the drop trigger so that the player can pass through it when they climb to the top
    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            dropTrigger.gameObject.SetActive(false);
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = new Color(0.2f, 1f, 0f, 0.5f);
        Gizmos.DrawCube(transform.position, transform.lossyScale);

        Gizmos.color = new Color(0f, 1f, 0f, 1f);
        Gizmos.DrawWireCube(transform.position, transform.lossyScale);
    }

    public void ResetDrop()
    {
        StartCoroutine(ActivateDrop());
    }

    //After the player has jumped off the ladder, they reset the drop trigger in case they fall down again
    IEnumerator ActivateDrop()
    {
        yield return new WaitForSeconds(0.3f);
        dropTrigger.gameObject.SetActive(true);
    }
}

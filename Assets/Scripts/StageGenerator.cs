﻿//~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
//StageGenerator.cs                 
//
//by: Nicholas MacDonald
//
//Randomly generates stages using a list of segments pieced together
//~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*

using UnityEngine;
using System.Collections;

public class StageGenerator : MonoBehaviour
{
    public float stageWidth; //The minimum width of a stage in units
    public float startPoint; //The local X point regarding where segments should be placed
    public float deathPitChance; //The chance for the stage to be a death pit type (crocs, swinging rope, ect)

    //Stage templates
    public GameObject lowerGroundStage, deathPitStage;

    public Segment[] groundSegments; //Segments that represent walkable ground
    public Segment[] segments; //Obstacle segments (pits, crocs, swinging rope, ladder)

    //The ladder is special as it's currently the only way to exit the lower level, meaning it will be required for lower ground stages
    public Segment ladder; 
    public Segment longGround; //A long ground segment to make sure we can reach the end of the level

    public GameObject barrel;

    //A list of weights for determining how many barrels spawn where the index is the number of barrels
    public float[] barrelCountChances;

    //The min and max delay before launching a barrel in seconds
    public float minBarrelDelayGap;
    public float maxBarrelDelayGap;

    //The size of the smallest segments for each level type
    private float smallestlowerGroundSegWidth;
    private float smallestdeathPitSegWidth;

    //The highest weight for barrel selection, to be compared against other weights
    private float highestBarrelChance = 0f;

    void Start()
    {
        //First we determine the smallest segment for each level type. This'll be used later to make sure we have
        // room to place an obstacle segment before we place it
        smallestlowerGroundSegWidth = 10000f;
        smallestdeathPitSegWidth = 10000f;

        for (int j = 0; j < segments.Length; j++)
        {
            if(segments[j].width < smallestlowerGroundSegWidth && segments[j].lowerGroundEnabled)
            {
                smallestlowerGroundSegWidth = segments[j].width;
            }

            if (segments[j].width < smallestdeathPitSegWidth && segments[j].deathPitEnabled)
            {
                smallestdeathPitSegWidth = segments[j].width;
            }
        }

        highestBarrelChance = 0f;

        //We find the highest barrel weight for later on when we're selecting barrel counts
        for(int j = 0; j < barrelCountChances.Length; j++)
        {
            if(barrelCountChances[j] > highestBarrelChance)
            {
                highestBarrelChance = barrelCountChances[j];
            }
        }
    }

    //La piece de resistance
    public GameObject GenerateStage()
    {
        //First, we determine if we're going to have a lower level or a death pit
        bool isDeathPit = Random.value < deathPitChance;
     
        GameObject stage;

        if (isDeathPit)
        {
            Debug.Log("Generating deathPit stage");
            stage = Instantiate(deathPitStage);
        }
        else
        {
            Debug.Log("Generating lowerGround stage");
            stage = Instantiate(lowerGroundStage);
        }

        //Next, we prepare the generation loop by establishing the starting point
        float distanceCovered = 0f;
        float currentPoint = startPoint;
        
        //We also start with an obstacle before a ground piece and we also state we haven't placed a ladder yet
        // (in case it's a lower level stage)
        bool groundSeg = false;
        bool ladderPlanted = false;

        //While we have not reached the end of the stage yet
        while (distanceCovered < stageWidth)
        {
            //If there is absolutely no room for an obstacle
            if(isDeathPit && stageWidth - distanceCovered <= smallestdeathPitSegWidth ||
                !isDeathPit && stageWidth - distanceCovered <= smallestlowerGroundSegWidth)
            {
                //We create a ground segment and finish the stage
                GameObject nextSegment = Instantiate(longGround.gameObject);
                nextSegment.transform.parent = stage.transform;

                nextSegment.transform.localPosition = new Vector3(currentPoint + longGround.width / 2f, 0f, 0f);

                //We just pass it the stage width to make sure the generater knows we're done
                distanceCovered += stageWidth;
            }
            //Else, if we're planting a ground segment
            else if (groundSeg)
            {
                Debug.Log("Adding Ground Segment");

                //We select a random segment. It doesn't matter if it's too long because it's harmless for a 
                // ground piece to extend out past the screen.
                Segment seg = groundSegments[Random.Range(0, groundSegments.Length)];

                float remainingRoom = stageWidth - distanceCovered - seg.width;

                //If placing the next segments results in there not being enough room for another obstacle
                if(isDeathPit && remainingRoom <= smallestdeathPitSegWidth ||
                !isDeathPit && remainingRoom <= smallestlowerGroundSegWidth)
                {
                    //We just opt to finish the stage instead
                    GameObject nextSegment = Instantiate(longGround.gameObject);
                    nextSegment.transform.parent = stage.transform;

                    nextSegment.transform.localPosition = new Vector3(currentPoint + longGround.width / 2f, 0f, 0f);

                    distanceCovered += stageWidth;
                }
                else
                {
                    //If we will have room for another object, we place the ground piece and progress the generator forward
                    GameObject nextSegment = Instantiate(seg.gameObject);
                    nextSegment.transform.parent = stage.transform;

                    nextSegment.transform.localPosition = new Vector3(currentPoint + seg.width / 2f, 0f, 0f);

                    currentPoint += seg.width;
                    distanceCovered += seg.width;

                    groundSeg = false;
                }
                
            }
            //Else, if we're placing an obstacle
            else
            {
                //If we're on stage with a lower level and we haven't put down a ladder yet
                if (!isDeathPit && !ladderPlanted)
                {
                    Debug.Log("Adding Ladder Segment");

                    //We force the placement of a ladder segment to make sure the player character can escape the lower pit.
                    GameObject nextSegment = Instantiate(ladder.gameObject);
                    nextSegment.transform.parent = stage.transform;

                    nextSegment.transform.localPosition = new Vector3(currentPoint + ladder.width / 2f, 0f, 0f);

                    currentPoint += ladder.width;
                    distanceCovered += ladder.width;

                    ladderPlanted = true;
                }
                //If we've got our ladder or we're on a death pit stage
                else
                {
                    Debug.Log("Adding Obstacle Segment");

                    bool good = false;

                    Segment seg = null;

                    //While we don't have a segment selected
                    while (!good)
                    {
                        //We pick a random segment
                        seg = segments[Random.Range(0, segments.Length)];

                        //If it's compatable with the current stage type and it fits, we're good
                        good = (isDeathPit && seg.deathPitEnabled || !isDeathPit && seg.lowerGroundEnabled) &&
                               distanceCovered + seg.width < stageWidth;

                        //If not, we do some quick debug reporting
                        if(!good)
                        {
                            if(isDeathPit && !seg.deathPitEnabled || !isDeathPit && !seg.lowerGroundEnabled)
                            {
                                Debug.Log("Incompatable segment!");
                            }

                            if(distanceCovered + seg.width > stageWidth)
                            {
                                Debug.Log("Segment too big!");
                            }
                        }
                    }

                    //Once we found a good segment, we place it and progress the generator
                    GameObject nextSegment = Instantiate(seg.gameObject);
                    nextSegment.transform.parent = stage.transform;

                    nextSegment.transform.localPosition = new Vector3(currentPoint + seg.width / 2f, 0f, 0f);

                    currentPoint += seg.width;
                    distanceCovered += seg.width;               
                }

                //After placing an obstacle, we go back to placing a road segment
                groundSeg = true;
            }
        }

        int numBarrels = -1;
        
        //While we haven't determined a number of barrels to place
        while(numBarrels == -1)
        {
            //We select a random amount of barrels based on the number of indicies in the array
            int count = Random.Range(0, barrelCountChances.Length);

            //Based on the highest weight, we use the weight of the randomly selected barrel count and roll to see if
            // that barrel count will be selected
            if(Random.Range(0, highestBarrelChance) < barrelCountChances[count])
            {
                //If it is selected, we set it to be the number of barrels to spawn, otherwise, we randomly select another index.
                numBarrels = count;
            }
        }

        //We set a random delay for the first barrel
        float delay = Random.Range(0f, maxBarrelDelayGap);

        //We spawn multiple barrels, set them to be children of the stage and set a random delay.
        for(int j = 0; j < numBarrels; j++)
        {
            GameObject nextBarrel = Instantiate(barrel);

            nextBarrel.transform.parent = stage.transform;

            Barrel barComponent = nextBarrel.GetComponent<Barrel>();
            barComponent.Reset();

            barComponent.delay = delay;

            delay += Random.Range(minBarrelDelayGap, maxBarrelDelayGap);
        }

        Debug.Log("Stage generation complete!");

        //We return the now generated stage
        return stage;
    }
}

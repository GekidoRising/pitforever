﻿//~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
//DeathTrigger.cs                 
//
//by: Nicholas MacDonald
//
//Draws a gizmo for the drop trigger so that it's visible
//~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*

using UnityEngine;
using System.Collections;

public class Drop : MonoBehaviour
{
    //Visualizes the drop trigger as a red cube
    void OnDrawGizmos()
    {
        Gizmos.color = new Color(1f, 0f, 0f, 0.5f);
        Gizmos.DrawCube(transform.position, transform.lossyScale);

        Gizmos.color = new Color(1f, 0.2f, 0f, 1f);
        Gizmos.DrawWireCube(transform.position, transform.lossyScale);
    }

    //I still really like Gizmos
}

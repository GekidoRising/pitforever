﻿//~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
//Crocodile.cs                 
//
//by: Nicholas MacDonald
//
//Controls the crocodiles opening and closing their mouths
//~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*

using UnityEngine;
using System.Collections;

public class Crocodile : MonoBehaviour
{
    public GameObject jawHinge;

    public bool open;

    public float openDuration = 1.5f;
    public float closeDuration = 3f;
    public float wiggleDuration = 1f;

    private Collider boxCollider;

    //Once the crocodile is enabled, it resets its jaw and starts the coroutine for opening and closing its mouth.
    void OnEnable()
    {
        if (open)
        {
            jawHinge.transform.eulerAngles = new Vector3(0f, 0f, -45f);
        }

        boxCollider = GetComponent<Collider>();

        StartCoroutine(JawLoop());
    }

    IEnumerator JawLoop()
    {
        while(true)
        {
            //If the mouth is currently cloused
            if(!open)
            {
                //Wait for a certain amount of time to give the player the chance to platform on the crocs
                yield return new WaitForSeconds(closeDuration - wiggleDuration);

                float timer = wiggleDuration;

                //A short time prior to the crocs opening their mouths, we play a quick wiggle animation as a danger indicator
                while(timer > 0f)
                {
                    timer -= Time.deltaTime;

                    //We use a sin wave to smooth out the animation
                    float angleCoefficient = timer / 0.333f * 2 * Mathf.PI;
                    jawHinge.transform.eulerAngles = new Vector3(0f, 0f, Mathf.Cos(angleCoefficient) * 5f - 5f);

                    yield return null;
                }

                //Once the timer has concluded we reset the jaw position to make sure it's not slightly out of position
                jawHinge.transform.eulerAngles = new Vector3(0f, 0f, -0f);

                timer = 0.1f;

                //We very quickly open the mouth
                while(timer > 0f)
                {
                    timer -= Time.deltaTime;

                    //The lerp is backwards because the timer counts down from full to zero instead of up from zero
                    jawHinge.transform.eulerAngles = new Vector3(0f, 0f, Mathf.Lerp(-45f, 0f, timer / 0.1f));

                    yield return null;
                }

                //Again, we manually set the final position after the lerp to make sure we're at exactly the right angle
                jawHinge.transform.eulerAngles = new Vector3(0f, 0f, -45f);

                //We change the collider to a trigger as disabling it causes the player to think they're still grounded
                boxCollider.isTrigger = true;

                open = true;
            }
            else
            {
                //Closing the mouth is simpler as it doesn't require a wiggle animaiton.
                yield return new WaitForSeconds(openDuration);

                float timer = 0.1f;

                //Once the yield command has finished up, we quickly close the mouth.
                while (timer > 0f)
                {
                    timer -= Time.deltaTime;

                    jawHinge.transform.eulerAngles = new Vector3(0f, 0f, Mathf.Lerp(0f, -45f, timer / 0.1f));

                    yield return null;
                }

                jawHinge.transform.eulerAngles = new Vector3(0f, 0f, -0f);
                boxCollider.isTrigger = false;

                open = false;
            }
        }
    }
}

﻿//~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
//RopeHinge.cs                 
//
//by: Nicholas MacDonald
//
//Swings the rope back and forth on a sin wave
//~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*

using UnityEngine;
using System.Collections;

public class RopeHinge : MonoBehaviour
{
    public float angleRange = 30f;
    public float period = 2f;

    private float coefficient = 0f;

	//Swings the rope back and forth
	void Update ()
    {
        coefficient += Time.deltaTime;

        float result = Mathf.Sin(coefficient / period * 2f * Mathf.PI);

        transform.eulerAngles = new Vector3(0.0f, 0.0f, angleRange * result);
	}
}

﻿//~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
//Segment.cs                 
//
//by: Nicholas MacDonald
//
//Helper class for the level generated containing info relevant to the stage generation 
//~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*

using UnityEngine;
using System.Collections;

public class Segment : MonoBehaviour
{
    //The width of the entire segment
    public float width;

    //These variables determine which stage types a given segment can spawn on, ie, crocs don't make
    // very much spawning on a stage where there's a platform below
    public bool lowerGroundEnabled;
    public bool deathPitEnabled;
}

PitForever is Pitfall recreated in Unity, extended to infinity by randomly generating levels.

After four introductory stages that demonstrate each of the mechanics in PitForever(ladder, barrels, crocs and swinging rope), PitForever generates random stages by piecing together Segments one after the other, where a Segment represents a small piece of a stage, be it ground, a pit, a ladder, crocs, or a rope swing. It also generates a random amount of barrels for each stage.

Currently, the game and stage generation is relatively simple, but can be easily expanded by creating more Segments and adding them to the StageGenerator.

Assets downloaded from the store include:
-Casual Game Sounds: https://www.assetstore.unity3d.com/en/#!/content/54116
-Pixel Bomb Textures: https://www.assetstore.unity3d.com/en/#!/content/7016
-Wood Barrel: https://www.assetstore.unity3d.com/en/#!/content/4427
-Free Steel Ladder Pack: https://www.assetstore.unity3d.com/en/#!/content/24892

All code was written by Nicholas MacDonald